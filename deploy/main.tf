terraform {
  backend "s3" {
    bucket         = "recipe-app-api-devops-tfstates"
    key            = "recipe-app.tfstate"
    region         = "ap-south-1"
    encrypt        = true
    dynamodb_table = "recipe-app-api-devops-tf-state-lock"
  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.65.0"
    }
  }
}

provider "aws" {
  region = "ap-south-1"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    Managedby   = "Terraform"
  }
}

data "aws_region" "current" {

}