variable "prefix" {
  default = "read"
}

variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "balimidi.ganesh@outlook.com"
}

variable "db_username" {
  description = "Username for the RDS postgres Instance"
}

variable "db_password" {
  description = "Password for the RDS postgres instance"
}

variable "bastion_key_name" {
  default = "recipe-app-api-devops-bastion"
}

variable "ecr_image_api" {
  description = "ECR Image fro API"
  default     = "402972435667.dkr.ecr.ap-south-1.amazonaws.com/recipe-app-api-devops:latest"
}

variable "ecr_image_proxy" {
  description = "ECR Image for Proxy"
  default     = "402972435667.dkr.ecr.ap-south-1.amazonaws.com/recipe-app-api-proxy.latest"
}

variable "django_secret_key" {
  description = "Secret key for Django app"
}



